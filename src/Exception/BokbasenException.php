<?php

namespace Drupal\bokbasen_checkout\Exception;

/**
 * Base exception for all bokbasen errors.
 */
class BokbasenException extends \Exception {}
