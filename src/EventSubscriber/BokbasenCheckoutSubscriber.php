<?php

namespace Drupal\bokbasen_checkout\EventSubscriber;

use Drupal\bokbasen_checkout\Service\BokbasenCheckoutManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Bokbasen checkout event subscriber.
 */
class BokbasenCheckoutSubscriber implements EventSubscriberInterface {

  const QUEUE_NAME = 'bokbasen_checkout_queue';

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * Bokbasen manager.
   *
   * @var \Drupal\bokbasen_checkout\Service\BokbasenCheckoutManager
   */
  protected $bokbasenManager;

  /**
   * Commerce log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * Constructs event subscriber.
   */
  public function __construct(QueueFactory $queue, EntityTypeManagerInterface $entity_type_manager, BokbasenCheckoutManager $bokbasen_manager) {
    $this->queue = $queue;
    $this->logStorage = $entity_type_manager->getStorage('commerce_log');
    $this->bokbasenManager = $bokbasen_manager;
  }

  /**
   * React on orders that are placed of the correct type.
   */
  public function queuePurchase(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $bokbasen_items = $this->bokbasenManager->extractItemsFromOrder($order);
    if ($bokbasen_items) {
      // Queue the order id, so we dont serialize the whole thing.
      $this->logStorage->generate($order, 'bokbasen_queued')->save();
      $this->queue->get(self::QUEUE_NAME)->createItem($order->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.post_transition' => ['queuePurchase', -100],
    ];
  }

}
