<?php

namespace Drupal\bokbasen_checkout\Service;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\user\UserDataInterface;

/**
 * Bokbasen checkout manager.
 */
class BokbasenCheckoutManager {

  const BOKBASEN_PRODUCT_TYPE = 'bokbasen';
  const BOKBASEN_ORDER_ITEM_TYPE = 'bokbasen';

  /**
   * User data manager.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * BokbasenCheckoutManager constructor.
   */
  public function __construct(UserDataInterface $user_data) {
    $this->userData = $user_data;
  }

  /**
   * Get bokbasen items from the order.
   */
  public function extractItemsFromOrder(OrderInterface $order) {
    $bokbasen_items = [];
    foreach ($order->getItems() as $item) {
      if ($item->bundle() === self::BOKBASEN_ORDER_ITEM_TYPE) {
        $bokbasen_items[] = $item;
      }
    }

    return $bokbasen_items;
  }

  /**
   * Get ddsid from the storage.
   */
  public function getUserDdsidFromStorage($uid) {
    return $this->userData->get('bokbasen_checkout', $uid, 'ddsid');
  }

  /**
   * Set ddsid to the storage.
   */
  public function setUserDdsidToStorage($uid, $ddsid) {
    return $this->userData->set('bokbasen_checkout', $uid, 'ddsid', $ddsid);
  }

  /**
   * Get user first name from the order.
   */
  public function getUserFirstName(OrderInterface $order) {
    /** @var \Drupal\user\UserInterface $customer */
    $customer = $order->getCustomer();
    if ($customer->isAuthenticated()) {
      return $customer->getAccountName();
    }
    if ($email = $order->getEmail()) {
      return strstr($email, '@', TRUE);
    }

    return 'First name';
  }

  /**
   * Get user last name from the order.
   */
  public function getUserLastName(OrderInterface $order) {
    /** @var \Drupal\user\UserInterface $customer */
    $customer = $order->getCustomer();
    if ($customer->isAuthenticated()) {
      return $customer->getAccountName();
    }
    if ($email = $order->getEmail()) {
      return strstr($email, '@', TRUE);
    }

    return 'Last name';
  }

  /**
   * Build order id for the Bokbasen API.
   */
  public function buildOrderId(OrderInterface $order, OrderItemInterface $order_item) {
    return sprintf('order-%s-item-%s', $order->id(), $order_item->id());
  }

}
