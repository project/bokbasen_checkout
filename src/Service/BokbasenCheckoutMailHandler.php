<?php

namespace Drupal\bokbasen_checkout\Service;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Bokbasen mail handler.
 */
class BokbasenCheckoutMailHandler {

  use StringTranslationTrait;

  const LINKS_GENERATED = 'bokbasen_links_generated';

  /**
   * Renderer manager.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * Sends the mails.
   */
  public function handleMail($key, &$message, $params) {
    if ($key === self::LINKS_GENERATED) {
      $this->handleLinks($key, $message, $params);
    }
  }

  /**
   * Book links mail.
   */
  protected function handleLinks($key, &$message, $params) {
    $message['subject'] = $this->t('Activation links for your purchase');
    $body = [
      '#theme' => 'bokbasen_checkout_activation_links_mail',
      '#urls' => $params['urls'],
      '#order_id' => $params['order_id'],
    ];
    $message['body'][] = $this->renderer->render($body);
  }

}
