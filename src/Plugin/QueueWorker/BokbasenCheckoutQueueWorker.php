<?php

namespace Drupal\bokbasen_checkout\Plugin\QueueWorker;

use Drupal\bokbasen_checkout\ApiClient\BokbasenBokskya;
use Drupal\bokbasen_checkout\ApiClient\BokbasenDistribution;
use Drupal\bokbasen_checkout\Exception\BokbasenException;
use Drupal\bokbasen_checkout\Service\BokbasenCheckoutMailHandler;
use Drupal\bokbasen_checkout\Service\BokbasenCheckoutManager;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends a message.
 *
 * @QueueWorker(
 *   id = "bokbasen_checkout_queue",
 *   title = @Translation("Process bokbasen relevant orders"),
 *   cron = {"time" = 60}
 * )
 */
class BokbasenCheckoutQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Bokbasen manager.
   *
   * @var \Drupal\bokbasen_checkout\Service\BokbasenCheckoutManager
   */
  protected $bokbasenManager;

  /**
   * Bokskya api client.
   *
   * @var \Drupal\bokbasen_checkout\ApiClient\BokbasenBokskya
   */
  protected $bokskyaApiClient;

  /**
   * Bokbasen distribution api client.
   *
   * @var \Drupal\bokbasen_checkout\ApiClient\BokbasenDistribution
   */
  protected $distributionApiClient;

  /**
   * Commerce log storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $logStorage;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * VitalsourceCheckoutWorker constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, MailManagerInterface $mail_manager, BokbasenCheckoutManager $bokbasen_manager, BokbasenBokskya $bokskya, BokbasenDistribution $bokbasen_distribution, TranslationManager $string_translation, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->bokbasenManager = $bokbasen_manager;
    $this->bokskyaApiClient = $bokskya;
    $this->distributionApiClient = $bokbasen_distribution;
    $this->stringTranslation = $string_translation;
    $this->logStorage = $this->entityTypeManager->getStorage('commerce_log');
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('plugin.manager.mail'),
      $container->get('bokbasen_checkout.manager'),
      $container->get('bokbasen_checkout.bokskya'),
      $container->get('bokbasen_checkout.distribution'),
      $container->get('string_translation'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Data should be an order id.
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    if (!$order = $this->entityTypeManager->getStorage('commerce_order')->load($data)) {
      return;
    }
    $this->logStorage->generate($order, 'bokbasen_start')->save();
    $bokbasen_items = $this->bokbasenManager->extractItemsFromOrder($order);
    if ($bokbasen_items) {
      try {
        $user_ddsid = $this->getUserDdsid($order);
        if ($user_ddsid && $this->bokskyaApiClient->validate($user_ddsid)) {
          $user_catalog = $this->distributionApiClient->getPersonalCatalog($user_ddsid);
          /** @var \Drupal\commerce_order\Entity\OrderItemInterface $bokbasen_item */
          foreach ($bokbasen_items as $bokbasen_item) {
            $this->processOrderItem($order, $bokbasen_item, $user_ddsid, $user_catalog);
          }
        }
        else {
          throw new BokbasenException($this->t('User dds id is not valid or user is blocked in Bokbasen system.'));
        }
      }
      catch (\Exception $e) {
        $this->logStorage->generate($order, 'bokbasen_error', ['error' => $e->getMessage()])->save();
        throw $e;
      }
    }
  }

  /**
   * Get existing ddsid or register a new user.
   */
  public function getUserDdsid(OrderInterface $order) {
    $ddsid = NULL;
    // Check if order contains ddsid.
    foreach ($this->bokbasenManager->extractItemsFromOrder($order) as $bokbasen_item) {
      if ($bokbasen_item->bokbasen_ddsid->value) {
        $ddsid = $bokbasen_item->bokbasen_ddsid->value;
      }
    }
    if ($ddsid) {
      return $ddsid;
    }
    // Check for user data.
    $customer_id = $order->getCustomerId();
    if ($ddsid = $this->bokbasenManager->getUserDdsidFromStorage($customer_id)) {
      return $ddsid;
    }
    $customer_email = $order->getEmail();
    // Check if user with email exists.
    $ddsid = $this->bokskyaApiClient->validate($customer_email);
    if ($ddsid) {
      $this->bokbasenManager->setUserDdsidToStorage($customer_id, $ddsid);
      return $ddsid;
    }
    $user_data = [
      'email' => $customer_email,
      'name' => $this->bokbasenManager->getUserFirstName($order),
      'surname' => $this->bokbasenManager->getUserFirstName($order),
    ];
    $ddsid = $this->bokskyaApiClient->register($user_data);
    if (empty($ddsid)) {
      throw new BokbasenException($this->t('Can not get a user dds id.'));
    }
    $this->bokbasenManager->setUserDdsidToStorage($customer_id, $ddsid);

    return $ddsid;
  }

  /**
   * Get content url.
   */
  public function getContentUrl(OrderInterface $order, OrderItemInterface $bokbasen_item, string $user_ddsid, string $inventory_id, array $bokbasen_catalog_product = []) {
    $content_url = NULL;
    $price = $bokbasen_item->getTotalPrice();
    $this->moduleHandler->alter('bokbasen_checkout_order_item_price', $price);
    // We only support NOK as currency, since that is what bokbasen orders
    // operate with.
    if (!$price || $price->getCurrencyCode() !== 'NOK') {
      throw new BokbasenException('Order item @id does not have a price in NOK. To be able to place this order, please implement hook_bokbasen_checkout_order_item_price_alter');
    }
    // Get content url from the user catalog.
    if (isset($bokbasen_catalog_product['link'][1]['@href'])) {
      $content_url = $bokbasen_catalog_product['link'][1]['@href'];
    }
    // Place a new order.
    else {
      $order_data = [
        'orderid' => $this->bokbasenManager->buildOrderId($order, $bokbasen_item),
        'resid' => $inventory_id,
        'spd' => (int) $price->multiply(100)->getNumber(),
        'id' => $user_ddsid,
        'firstname' => $this->bokbasenManager->getUserFirstName($order),
        'lastname' => $this->bokbasenManager->getUserLastName($order),
        'email' => $order->getEmail(),
      ];
      $content_url = $this->distributionApiClient->placeOrder($order_data);
    }
    if (!$content_url) {
      throw new BokbasenException($this->t('Can not get a content URL.'));
    }

    return $content_url;
  }

  /**
   * Process bokbasen order item.
   */
  public function processOrderItem(OrderInterface $order, OrderItemInterface $bokbasen_item, string $user_ddsid, array $user_catalog) {
    $bokbasen_item->set('bokbasen_ddsid', $user_ddsid);
    $product = NULL;
    $item = $bokbasen_item->getPurchasedEntity();
    $sku = $item->getSku();
    $inventory = $this->distributionApiClient->getInventory($sku);
    if (!$inventory) {
      throw new BokbasenException($this->t('Inventory for the SKU: @sku could not be fetched.', ['@sku' => $sku]));
    }
    $inventory = reset($inventory);
    $inventory_id = str_replace('urn:uuid:', '', $inventory['id']);
    // Check if product in catalog.
    $bokbasen_catalog_product = [];
    foreach ($user_catalog as $catalog_item) {
      if ($catalog_item['id'] === $inventory_id) {
        $bokbasen_catalog_product = $catalog_item;
      }
    }
    // Save fulfilment id into the product item and send mail.
    $content_url = $this->getContentUrl($order, $bokbasen_item, $user_ddsid, $inventory_id, $bokbasen_catalog_product);
    $base_url = $this->distributionApiClient->getBaseUrl();
    // Fetch id only for storing and api call.
    $fulfilment_id = str_replace($base_url . '/' . BokbasenDistribution::CONTENT_PATH . '/', '', $content_url);
    $bokbasen_item->set('bokbasen_fulfilmentid', $fulfilment_id);
    $bokbasen_item->save();
    $download_url = $this->distributionApiClient->getDownloadContentLink($fulfilment_id);
    if (!$download_url) {
      throw new BokbasenException('No download URL was found for fulfillment id ' . $fulfilment_id);
    }
    $status = $this->sendMailWithDownloadUrl($order, $download_url);

    return $status;
  }

  /**
   * Send mail with the download url.
   */
  public function sendMailWithDownloadUrl(OrderInterface $order, string $download_url) {
    $mail_language = $this->languageManager->getDefaultLanguage()->getId();
    if ($owner = $order->get('uid')) {
      // See if we can load that user.
      if (!empty($owner->entity)) {
        /** @var \Drupal\user\UserInterface $owner_user */
        $owner_user = $owner->entity;
        $mail_language = $owner_user->getPreferredLangcode();
      }
    }
    $mail_params = [
      'urls' => [
        'download_link' => [
          'url' => $download_url,
          'title' => $this->t('Download resource'),
        ],
      ],
      'order_id' => $order->id(),
    ];
    $this->mailManager->mail('bokbasen_checkout', BokbasenCheckoutMailHandler::LINKS_GENERATED, $order->getEmail(), $mail_language, $mail_params);
    $this->logStorage->generate($order, 'bokbasen_success')->save();
    $this->logStorage->generate($order, 'bokbasen_email_sent', ['email' => $order->getEmail()])->save();

    return TRUE;
  }

}
