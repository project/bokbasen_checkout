<?php

namespace Drupal\bokbasen_checkout\ApiClient;

use Bokbasen\ApiClient\Client;
use Bokbasen\ApiClient\HttpRequestOptions;
use Bokbasen\Auth\Login;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\serialization\Encoder\JsonEncoder;
use Drupal\serialization\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class BokbasenApiClientFactory.
 */
abstract class BokbasenApiClientBase implements BokbasenApiClientInterface {

  use StringTranslationTrait;

  const BASE_USERNAME = 'bokbasen_username';
  const BASE_PASSWORD = 'bokbasen_password';

  /**
   * {@inheritdoc}
   */
  abstract public function getBaseUrl();

  /**
   * Api client.
   *
   * @var \Bokbasen\ApiClient\Client
   */
  protected $client;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger = NULL;


  /**
   * Json Encoder.
   *
   * @var \Drupal\serialization\Encoder\JsonEncoder
   */
  protected $jsonEncoder;

  /**
   * XML encoder.
   *
   * @var \Drupal\serialization\Encoder\XmlEncoder
   */
  protected $xmlEncoder;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a BokbasenApiClient object.
   */
  public function __construct(JsonEncoder $json_encoder, XmlEncoder $xml_encoder, LoggerChannelFactoryInterface $logger, ModuleHandlerInterface $module_handler, TranslationManager $string_translation) {
    $this->jsonEncoder = $json_encoder;
    $this->xmlEncoder = $xml_encoder;
    $this->stringTranslation = $string_translation;
    $this->moduleHandler = $module_handler;
    $this->logger = $logger->get('bokbasen_checkout');
    $serializer = new Serializer([new GetSetMethodNormalizer()]);
    $this->xmlEncoder->setSerializer($serializer);
    $login_url = $this->getLoginUrl();
    $credentials = [
      'username' => Settings::get(self::BASE_USERNAME),
      'password' => Settings::get(self::BASE_PASSWORD),
    ];
    $this->moduleHandler->alter('bokbasen_login_credentials', $credentials, $this);
    $login = new Login($credentials['username'], $credentials['password'], $login_url);
    $base_url = $this->getBaseUrl();
    if ($base_url) {
      $this->client = new Client($login, $base_url);
    }
    if ($this->isTestMode()) {
      $this->client->setLogger($this->logger);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequest(string $path, array $headers = [], string $encode_format = JsonEncoder::FORMAT) {
    $results = [];
    if (!isset($headers['Accept']) && $encode_format === JsonEncoder::FORMAT) {
      $headers['Accept'] = HttpRequestOptions::CONTENT_TYPE_JSON;
    }
    $response = $this->getClient()->get($path, $headers);
    if ($response->getStatusCode() == 200) {
      $results = $response->getBody()->getContents();
    }
    // Decode json.
    if ($results && $encode_format === JsonEncoder::FORMAT) {
      return $this->jsonEncoder->decode($results, JsonEncoder::FORMAT);
    }
    // Decode XML.
    if ($results && $encode_format === 'xml') {
      return $this->xmlEncoder->decode($results, $encode_format);
    }

    if ($response->getStatusCode() == 302) {
      if ($response->hasHeader('Location')) {
        return $response->getHeader('Location');
      }
    }
    else {
      $this->logger->notice($response->getBody());
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function jsonPostRequest(string $path, array $body, array $headers = [], string $encode_format = JsonEncoder::FORMAT) {
    if (!isset($headers['Accept']) && $encode_format === JsonEncoder::FORMAT) {
      $headers['Accept'] = HttpRequestOptions::CONTENT_TYPE_JSON;
    }
    $response = $this->getClient()->postJson($path, $body);
    $results = $response->getBody();
    if ($response->getStatusCode() == 200 && $encode_format === JsonEncoder::FORMAT) {
      return $this->jsonEncoder->decode($results, JsonEncoder::FORMAT);
    }
    if ($response->getStatusCode() == 201) {
      if ($response->hasHeader('Location')) {
        return $response->getHeader('Location');
      }

      return TRUE;
    }
    else {
      $this->logger->notice($results->getContents());
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLoginUrl() {
    $is_test_mode = $this->isTestMode();

    return $is_test_mode ? Login::URL_TEST : Login::URL_PROD;
  }

  /**
   * {@inheritdoc}
   */
  public function isTestMode() {
    return Settings::get('bokbasen_test_mode', FALSE);
  }

}
