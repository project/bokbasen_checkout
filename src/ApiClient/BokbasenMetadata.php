<?php

namespace Drupal\bokbasen_checkout\ApiClient;

use Drupal\Core\Site\Settings;

/**
 * Bokbasen metadata API implementation.
 */
class BokbasenMetadata extends BokbasenApiClientBase {

  const API_PATH_PREFIX = 'metadata';
  const DATE_FORMAT = 'YmdHis';

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    return Settings::get('bokbasen_metadata_api_url', 'https://api.boknett.no');
  }

  /**
   * Export products.
   */
  public function onixProductsExport($page_size = 100, string $after = '', string $next = '', string $subscription = '', bool $decode = TRUE) {
    $results = [];
    if (empty($subscription)) {
      $subscription = $this->getSubscriptionType();
    }
    if (!$after && !$next) {
      $after = $this->getAfterDate();
    }
    $query = sprintf('subscription=%s&pagesize=%s', $subscription, $page_size);
    if ($after && !$next) {
      $query .= sprintf('&after=%s', $after);
    }
    if (!$after && $next) {
      $query .= sprintf('&next=%s', $next);
    }
    $response = $this->getClient()->get(sprintf('/metadata/export/onix?%s', $query));
    if ($response->getStatusCode() == 200) {
      $content = $response->getBody()->getContents();
      $results['results'] = $decode ? $this->xmlEncoder->decode($content, 'xml') : $content;
      if ($response->hasHeader('Link')) {
        $results['next'] = $response->getHeader('Next');
      }
    }
    else {
      $this->logger->error(sprintf('Something went wrong %s: %s. %s', $response->getStatusCode(), $response->getReasonPhrase(), $response->getBody()->getContents()));
    }

    return $results;
  }

  /**
   * Get subscription type.
   */
  public function getSubscriptionType() {
    return Settings::get('bokbasen_metadata_api_subscription', 'basic');
  }

  /**
   * Get after start date value.
   *
   * @return string
   *   Format yyyyMMddHHmmss.
   */
  public function getAfterDate() {
    return Settings::get('bokbasen_metadata_api_after_start', '20000109130000');
  }

}
