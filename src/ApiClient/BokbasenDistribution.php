<?php

namespace Drupal\bokbasen_checkout\ApiClient;

use Drupal\bokbasen_checkout\Exception\BokbasenException;
use Drupal\Core\Site\Settings;

/**
 * Bokbasen Distribution API implementation.
 */
class BokbasenDistribution extends BokbasenApiClientBase {

  const API_VERSION = 'v2';
  const CONTENT_STATUS_READY = 'READY';
  const CONTENT_STATUS_FAILED = 'FAILED';
  const CONTENT_PATH = 'content';

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    return Settings::get('bokbasen_distribution_api_url', 'https://api.dds.boknett.no');
  }

  /**
   * Get inventory.
   *
   * Next or after should be provided when no isbn.
   */
  public function getInventory(string $isbn = NULL, string $after = NULL, string $next = NULL) {
    $query = '';
    if (!$isbn) {
      $query = sprintf('after=%s&next=%s', $after, $next);
    }
    $results = $this->getRequest(sprintf('/%s/%s/%s?%s', self::API_VERSION, 'inventory', $isbn, $query), [], 'xml');
    if (!empty($results['entry'])) {
      if (isset($results['entry']['id'])) {
        $results['entry'] = [
          $results['entry'],
        ];
      }

      return $results['entry'];
    }

    return [];
  }

  /**
   * Place order.
   *
   * @param array $order_data
   *   User data.
   *   array(
   *      'orderid' => '1111' - store internal Id*
   *      'resid' => '2222' - product DDS ID*
   *      'spd' => '21' - price*
   *      'id' => 'dsd3-324ds-d' - DDS ID foe end consumer
   *      'firstname' => 'example name' - first name of person who buying
   *      'lastname' => 'example_name' - last name of person who buying
   *      'excerpt' => 1 - is for excerpt instead of actual book(defaults to 0)
   *      'email' => optional, for internal usage
   *   )
   *
   * @return mixed
   *   Content id or FALSE.
   */
  public function placeOrder(array $order_data) {
    $headers['Content-type'] = 'application/json';
    $this->moduleHandler->alter('bokbasen_order', $order_data);
    $results = $this->jsonPostRequest(sprintf('/%s', 'order'), $order_data, $headers);
    if (is_array($results)) {
      return reset($results);
    }
    // User cant buy this product.
    $this->logger->notice(
      $this->t(
        'User with email: %email cant buy product: %product_id within order: %order',
        [
          '%email' => $order_data['email'] ?: 'not set',
          '%product_id' => $order_data['resid'],
          '%order' => $order_data['orderid'],
        ]
      )
    );

    return $results;
  }

  /**
   * Get personal bookshelf for DDS user in OPDS XML format.
   *
   * @return array
   *   Entries.
   *   Entry = array(
   *    title => Product title,
   *    link => array(),
   *    author => array(name => Bob Hunter),
   *    id => 0a36837c-991b-4f10-99bb-a7caddeae52a,
   *    created => 2019-05-28T14:09:21.000Z,
   *    updated => 2010-03-04T14:03:16.000Z,
   *    dc:identifier => array(),
   *    dc:publisher => Universitetsforl.,
   *    dc:issued => 2010,
   *    dc:language => nob,
   *   )
   */
  public function getPersonalCatalog(string $ddsid) {
    $results = $this->getRequest(sprintf('/%s/%s/%s', 'catalog', 'personal', $ddsid), [], 'xml');
    if (!empty($results['entry'])) {
      // Check if only one.
      if (isset($results['entry']['id'])) {
        $results['entry'] = [
          $results['entry'],
        ];
      }
      return $results['entry'];
    }

    return [];
  }

  /**
   * Get download link for the content.
   */
  public function getDownloadContentLink(string $fulfilment_id) {
    $status_link = $this->getRequest(sprintf('/%s/%s', self::CONTENT_PATH, $fulfilment_id));
    $download_link = NULL;
    if ($status_link) {
      $status_link = reset($status_link);
      $status = NULL;
      $results = [];
      while ($status !== self::CONTENT_STATUS_READY) {
        $base_url = $this->getBaseUrl();
        $status_link = str_replace($base_url, '', $status_link);
        $results = $this->getRequest($status_link);
        $status = $results['status'];
        // Failed.
        if ($status === self::CONTENT_STATUS_FAILED) {
          break;
        }
        sleep(1);
      }
      if ($status === self::CONTENT_STATUS_READY) {
        $download_link = $results['downloadURL'];
      }
      else {
        throw new BokbasenException($this->t('Bokbasen can not fetch a download link for the fulfilmentid: %id.', ['%id' => $fulfilment_id]));
      }
    }

    return $download_link;
  }

}
