<?php

namespace Drupal\bokbasen_checkout\ApiClient;

use Drupal\serialization\Encoder\JsonEncoder;

/**
 * Defines a common interface for Bokbasen Api Clients.
 */
interface BokbasenApiClientInterface {

  /**
   * Get base url for client.
   *
   * @return string
   *   Base endpoint url.
   */
  public function getBaseUrl();

  /**
   * Get api client.
   *
   * @return \Bokbasen\ApiClient\Client
   *   Bokbasen client.
   */
  public function getClient();

  /**
   * Perform GET request.
   */
  public function getRequest(string $path, array $headers = [], string $encode_format = JsonEncoder::FORMAT);

  /**
   * Perform post json request.
   */
  public function jsonPostRequest(string $path, array $body, array $headers = [], string $encode_format = JsonEncoder::FORMAT);

  /**
   * Get login url.
   *
   * @return string
   *   Login URL.
   */
  public function getLoginUrl();

  /**
   * Check if test mode is enabled.
   *
   * @return bool
   *   Is test mode result.
   */
  public function isTestMode();

}
