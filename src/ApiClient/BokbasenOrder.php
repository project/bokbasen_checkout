<?php

namespace Drupal\bokbasen_checkout\ApiClient;

use Drupal\Core\Site\Settings;

/**
 * Bokbasen Order API implementation.
 */
class BokbasenOrder extends BokbasenApiClientBase {

  const API_PATH_PREFIX = 'orders';

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    return Settings::get('bokbasen_content_api_url', 'https://api.order.boknett.no');
  }

  /**
   * Get orders.
   */
  public function getOrders() {
    // TODO process response.
    $response = $this->getRequest(sprintf('/%s', self::API_PATH_PREFIX));
  }

  /**
   * Get specific order.
   */
  public function getOrder(string $order_id) {
    $response = $this->getRequest(sprintf('/%s/%s', self::API_PATH_PREFIX, $order_id));
  }

  /**
   * Get the EDItX XML representation of a given order.
   */
  public function getOrderEditx(string $order_id) {
    $response = $this->getRequest(sprintf('/%s/%s/%s', self::API_PATH_PREFIX, $order_id, 'editx'));
  }

}
