<?php

namespace Drupal\bokbasen_checkout\ApiClient;

use Drupal\Core\Site\Settings;

/**
 * Bokskya API implementation.
 */
class BokbasenBokskya extends BokbasenApiClientBase {

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    return Settings::get('bokbasen_bokskya_api_url', 'https://idp.dds.boknett.no');
  }

  /**
   * Validate state for a Bokskya account by ddsId or email.
   */
  public function validate(string $id) {
    $active = FALSE;
    $result = $this->getRequest(sprintf('/%s/%s', 'validate', $id));
    if (isset($result['id']) && $result['active']) {
      return $result['id'];
    }

    return $active;
  }

  /**
   * Create a new DDS Bokskya user account.
   *
   * @param array $user_data
   *   User data array(
   *      'email' => 'example@mail.com',
   *      'name' => 'example_name',
   *      'surname' => 'example_surname')
   *
   * @return mixed
   *   User id or FALSE.
   */
  public function register(array $user_data) {
    $this->moduleHandler->alter('bokbasen_user_register', $user_data);
    $result = $this->jsonPostRequest(sprintf('/%s', 'register'), $user_data);
    if (isset($result['id'])) {
      return $result['id'];
    }

    return FALSE;
  }

}
