<?php

/**
 * @file
 * Describes hooks provided by the Bokbasen Checkout module.
 */

use Drupal\bokbasen_checkout\ApiClient\BokbasenApiClientInterface;
use Drupal\bokbasen_checkout\ApiClient\BokbasenMetadata;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Bokbasen login credentials alter.
 *
 * @param array $credentials
 *   Client credentials.
 * @param \Drupal\bokbasen_checkout\ApiClient\BokbasenApiClientInterface $client
 *   Api client instance.
 */
function hook_bokbasen_login_credentials_alter(array &$credentials, BokbasenApiClientInterface $client) {
  if ($client instanceof BokbasenMetadata) {
    $credentials['username'] = 'metadata_user';
  }
}

/**
 * Alter order data which is placed to Bokbasen.
 *
 * @param array $order_data
 *   Order data which is send to Bokbasen.
 */
function hook_bokbasen_order(array $order_data) {
  $order_data['orderid'] .= $order_data['orderid'] . 'new';
}

/**
 * Alter user data which be sent to Bokbasen.
 *
 * @param array $user_data
 *   User data.
 */
function hook_bokbasen_user_register(array $user_data) {
  $user_data['new_field'] = 'User title';
}

/**
 * @} End of "addtogroup hooks".
 */
