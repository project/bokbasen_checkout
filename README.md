## Bokbasen API integration
API Doc: https://bokbasen.jira.com/wiki/spaces/api/overview

Settings which could be set in the settings.php: <br/>
Required and without default values<br/>
`bokbasen_username`<br/>
`bokbasen_password`<br/>
Endpoint URLs with defualt values<br/>
`bokbasen_test_mode` - enable test mode<br/>
`bokbasen_bokskya_api_url`<br/>
`bokbasen_content_api_url`<br/>
`bokbasen_distribution_api_url`<br/>
`bokbasen_metadata_api_url`<br/>
`bokbasen_content_api_url`<br/>
`bokbasen_metadata_api_after_start`<br/>
`bokbasen_metadata_api_subscription`<br/>

Login integration works with **v1** of Bokbasen API. <br/>
For the Bokbasen Inventory we use API **v2** endpoint.
<br/>
### Dependencies:<br/>
`bokbasen/php-api-client:2.x`<br/>
`nyholm/psr7:1.x`<br/>
