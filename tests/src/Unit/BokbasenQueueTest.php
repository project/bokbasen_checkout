<?php

namespace Drupal\Tests\bokbasen_checkout\Unit;

use Drupal\bokbasen_checkout\ApiClient\BokbasenBokskya;
use Drupal\bokbasen_checkout\ApiClient\BokbasenDistribution;
use Drupal\bokbasen_checkout\Exception\BokbasenException;
use Drupal\bokbasen_checkout\Plugin\QueueWorker\BokbasenCheckoutQueueWorker;
use Drupal\bokbasen_checkout\Service\BokbasenCheckoutManager;
use Drupal\commerce_log\Entity\Log;
use Drupal\commerce_log\LogStorage;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\OrderStorage;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use PHPUnit\Framework\TestCase;

/**
 * Test the queue worker.
 *
 * @group bokbasen_checkout
 */
class BokbasenQueueTest extends TestCase {

  /**
   * Order item.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject
   */
  private $orderItem;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->languageManager = $this->createMock(LanguageManager::class);
    $this->languageManager->method('getDefaultLanguage')
      ->willReturn(new Language(['id' => 'xyz']));
    $this->mailManager = $this->createMock(MailManagerInterface::class);
    $this->bokbasenManager = $this->createMock(BokbasenCheckoutManager::class);
    $this->bokskya = $this->createMock(BokbasenBokskya::class);
    $this->bokskya->method('validate')
      ->willReturn(TRUE);
    $this->distribution = $this->createMock(BokbasenDistribution::class);
    $this->distribution->method('getBaseUrl')
      ->willReturn('http://example.com');
    $this->distribution->method('placeOrder')
      ->with([
        'orderid' => NULL,
        'resid' => '123',
        'spd' => 12345,
        'id' => 'mock_id',
        'firstname' => NULL,
        'lastname' => NULL,
        'email' => NULL,
      ])
      ->willReturn('http://example.com/content/9876');
    $this->distribution->method('getDownloadContentLink')
      ->willReturn('http://example.com/download/9876');
    $this->distribution->method('getPersonalCatalog')
      ->willReturn([]);
    $this->distribution->method('getInventory')
      ->willReturn([
        [
          'id' => 'urn:uuid:123',
        ],
      ]);
    $this->moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $this->stringsTranslation = $this->createMock(TranslationManager::class);
    $order_storage = $this->createMock(OrderStorage::class);
    $mock_order = $this->createMock(Order::class);
    $order_storage->method('load')
      ->willReturn($mock_order);
    $mock_log_entry = $this->createMock(Log::class);
    $mock_log = $this->createMock(LogStorage::class);
    $mock_log->method('generate')
      ->willReturn($mock_log_entry);
    $this->entityTypeManager->method('getStorage')
      ->willReturnCallback(function ($type) use ($mock_log, $order_storage) {
        if ($type === 'commerce_log') {
          return $mock_log;
        }
        return $order_storage;
      });
    $this->orderItem = $this->createMock(OrderItem::class);
    $this->orderItem->method('__get')
      ->with('bokbasen_ddsid')
      ->willReturn((object) [
        'value' => 'mock_id',
      ]);
    $mock_variation = $this->createMock(ProductVariation::class);
    $mock_variation->method('getSku')
      ->willReturn(123);
    $this->orderItem->method('getPurchasedEntity')
      ->willReturn($mock_variation);
    $this->bokbasenManager->method('extractItemsFromOrder')
      ->willReturn([$this->orderItem]);
  }

  /**
   * Test that we are saving items.
   */
  public function testBokbasenItemSave() {
    $this->orderItem->method('getTotalPrice')
      ->willReturn(new Price('123.45', 'NOK'));
    $this->orderItem->expects($this->once())
      ->method('save');
    $queue = new BokbasenCheckoutQueueWorker([], 'plugin', [], $this->entityTypeManager, $this->languageManager, $this->mailManager, $this->bokbasenManager, $this->bokskya, $this->distribution, $this->stringsTranslation, $this->moduleHandler);
    $queue->processItem(123);
  }

  /**
   * Test that we are only supporting NOK.
   */
  public function testBokbasenItemCrashByCurrency() {
    $this->orderItem->method('getTotalPrice')
      ->willReturn(new Price('123.45', 'USD'));
    $this->expectException(BokbasenException::class);
    $this->expectExceptionMessage('Order item @id does not have a price in NOK. To be able to place this order, please implement hook_bokbasen_checkout_order_item_price_alter');
    $queue = new BokbasenCheckoutQueueWorker([], 'plugin', [], $this->entityTypeManager, $this->languageManager, $this->mailManager, $this->bokbasenManager, $this->bokskya, $this->distribution, $this->stringsTranslation, $this->moduleHandler);
    $queue->processItem(123);
  }

}
