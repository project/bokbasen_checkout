<?php

namespace Drupal\Tests\bokbasen_checkout\Unit;

use Drupal\bokbasen_checkout\Service\BokbasenCheckoutManager;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserDataInterface;
use Drupal\user\UserInterface;

/**
 * @coversDefaultClass \Drupal\bokbasen_checkout\Service\BokbasenCheckoutManager
 *
 * @group bokbasen_checkout
 */
class BokbasenCheckoutManagerTest extends UnitTestCase {

  /**
   * Bokbasen manager.
   *
   * @var \Drupal\bokbasen_checkout\Service\BokbasenCheckoutManager
   */

  protected $bokbasenManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $user_data_mock = $this->createMock(UserDataInterface::class);
    $this->bokbasenManager = new BokbasenCheckoutManager($user_data_mock);
  }

  /**
   * @covers ::buildOrderId
   */
  public function testBuildOrderId() {
    $order = $this->prophesize(OrderInterface::class);
    $order->id()->willReturn(21);
    $order_item = $this->prophesize(OrderItemInterface::class);
    $order_item->id()->willReturn(7);
    $built_id = $this->bokbasenManager->buildOrderId($order->reveal(), $order_item->reveal());
    $this->assertEquals('order-21-item-7', $built_id);
  }

  /**
   * Test const for product type.
   */
  public function testBokbasenEntityType() {
    $this->assertEquals('bokbasen', $this->bokbasenManager::BOKBASEN_PRODUCT_TYPE);
  }

  /**
   * @covers ::getUserLastName
   */
  public function testGetUserLastName() {
    $customer = $this->prophesize(UserInterface::class);
    $customer->isAuthenticated()->willReturn(TRUE);
    $customer->getAccountName()->willReturn('snow');
    $order = $this->prophesize(OrderInterface::class);
    $order->getCustomer()->willReturn($customer->reveal());
    $name = $this->bokbasenManager->getUserLastName($order->reveal());
    $this->assertEquals('snow', $name);

    $customer->isAuthenticated()->willReturn(FALSE);
    $order->getCustomer()->willReturn($customer->reveal());
    $order->getEmail()->willReturn('jon.snow@king.com');
    $name = $this->bokbasenManager->getUserLastName($order->reveal());
    $this->assertEquals('jon.snow', $name);
  }

  /**
   * @covers ::getUserFirstName
   */
  public function testGetUserFirstName() {
    $customer = $this->prophesize(UserInterface::class);
    $customer->isAuthenticated()->willReturn(TRUE);
    $customer->getAccountName()->willReturn('jon');
    $order = $this->prophesize(OrderInterface::class);
    $order->getCustomer()->willReturn($customer->reveal());
    $name = $this->bokbasenManager->getUserFirstName($order->reveal());
    $this->assertEquals('jon', $name);

    $customer->isAuthenticated()->willReturn(FALSE);
    $order->getCustomer()->willReturn($customer->reveal());
    $order->getEmail()->willReturn('jon.snow@king.com');
    $name = $this->bokbasenManager->getUserFirstName($order->reveal());
    $this->assertEquals('jon.snow', $name);
  }

}
